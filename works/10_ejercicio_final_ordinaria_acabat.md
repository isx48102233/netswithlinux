Una organización está organizada en dos departamentos:
- deptA
- deptB

Cada departamento ha de estar aislado del otro y ha de poder acceder
a una red de servidores que llamaremos "servers". También se dipone de 
una red wifi que no ha de poder acceder a las redes internas, y que sólo debe dejar
acceder al puerto 80 y 443 del servidor web de la red de servers. Desde
la red wifi también se podrá navegar a internet usando los puertos destino 80 y 443, pero
no se podrán establecer conexiones telnet, ssh o ftp.

Se ha contratado una línea empresarial a un ISP, que ha de ser utilizada
sólo por los servidores, excepto en caso de caída del "router inf" que
es el que se usa habitulamente como router de salida de las redes de 
los departamentos y de los dispositivos conectados por wifi.

Se ha de priorizar el tráfico del deptA respecto al deptB y se ha de 
repartir el ancho de banda por igual entre los usuarios de cada departamento.

Se ha de limitar el tráfico de bajada de la wifi a 500Kbps

El servidor ha de ser accesible desde inernet usando el puerto externo 77XX

Se ha de limitar el acceso por ssh (cambiando el puerto al 12322) al 
router desde cualquier red, y sólo se ha de poder hacer telnet desde la red de control

Se disone de un mikrotik de 4 puertos y de 6 puertos del switch para cada
puesto de trabajo que se conectarán de la siguiente forma:


MIKROTIK                   tagged               untagged   destino 
--------------------------------------------------------------------------
puerto 1   internet        escola,isp                      sw-puerto4 
puerto 2   puerto usb pc                                   pc-usb-ethernet 
puerto 3   redes internas  deptA,deptB,servers             sw-puerto5 
puerto 4      

     
SWITCH                   tagged          untagged type destino
-------------------------------------------------------------------------------------------
puerto 1   deptA                                         deptA      access    PC
puerto 2   detpB                                         deptB      access    PC
puerto 3   servers                                       servers    access    PC
puerto 4   mkt-internet         escola,isp                          trunk     mkt-puerto1
puerto 5   mkt-xarxes_internes  deptA,deptB,servers                 trunk     mkt-puerto3
puerto 6   roseta               deptA,deptB,servers,isp  escola     hybrid    roseta


Tabla de redes, ips y vlans:

REDES      VLAN      NETWORK-ADDR    IP-ADDR-MKT   DHCP-RANGE
---------------------------------------------------------------
deptA      7xx      10.27.1XX.0/24   .1            .100 - .200
deptB      8xx      10.28.1XX.0/24   .1            .100 - .200
servers    9xx      10.29.1XX.0/24   .1            NO DHCP
escola     1000     192.168.0.0/16   .3.2XX        NO DHCP
isp        1005       10.30.0.0/24   .2XX          NO DHCP
wireless   NO-VLAN  10.50.1XX.0/24   .1            .10 - .250
gestion    NO-VLAN 192.168.88.0/24   .1            NO DHCP


El esquema a nivel de capa 3 es el siguiente.


                    +-----------+             XX XXXXX XXXX
                    |router inf +----------+XXXXXX         XX
                    +-----------+        XX                 X
                          |.1            XX   internet      X  +-------------+
                          |               X              XXXX  |  router ISP |
             -------------+----+-+        XXXXXXXXXXXXXXX+-----+             |
               192.168.0.0/16  |                               +-------------+
                               |                                    |.1
                               |             10.30.0.0/24           |
                               |    +-+-----------------------------++
  +------+                     |      |
  |      |              escola |      | isp
  |server|            vlan1000 |      | vlan1005
  |web   |               port1 |      | port1
  +------+               .3.2XX|      |.2xx
     |.100                  +-------------+
     |                      |             |       +))
     |    10.29.1xx.0/24  .1|             |.1     |wireless
+----+----------------------+   MIKROTIK  +-------+
            vlan9xx    port3|             |    10.50.1xx.0/24
            servers         |             |
                            +--+-----+----+
                            .1 |     |  .1
                       vlan7xx |     |  vlan8xx
                         port3 |     |  port3
                         deptA |     |  deptB
                               |     |
                               |     |
      +------------------------+-+   |
          10.27.1xx.0/24           +-+------------------------------+
                                         10.28.1xx.0/24


# Resolución

Pasos:
1. Configurar switch
2. Configurar interfaces virtuales y direcciones IP en el router
3. Configurar servidores dhcp
4. Definir default gateway por router inf y verificar que salen, cambiar
default gateway por router-isp y verificar que salen. Dejar la ruta con más 
peso a router-inf. Verificar que al desconectar router-inf se cambia la salida por
router-isp
5. Activar las marcas de rutas para que desde la vlan de servers se salga
por router-isp y desde el resto de redes se salga por router-inf
6. Reglas de firewall para evitar accesos desde deptA a deptB y desde
wifi solo a server-web y a internet
7. Reglas de firewall para que sólo haya telnet desde ips de gestión 
y servidor ssh por puerto 12322 
8. Limitar ancho de banda de bajada en wireless a 500Kbps
9. Priorizar tráfico de descarga del deptA respecto a deptB
10. Repartir por igual el tráfico entre todos los usuarios de deptA y deptB

# RESOLUCION #

no spanning-tree vlan 2-1005

# 1. Configurar switch #
!PUERTO 1 DEPT A
interface fa0/7
switchport mode access
switchport access vlan 717

# PARTE ADRI #
!PUERTO 2 DEPT B
interface fa0/8
switchport mode access
switchport access vlan 817

!PUERTO 3 SERVERS
interface fa0/9
switchport mode access
switchport access vlan 917

!PUERTO 4 TRUNK MKT SALIDA INTERNET
interface fa0/10
switchport mode trunk
switchport trunk allowed vlan 1000,1005

!PUERTO 5 TRUNK MKT REDES INTERNAS
interface fa0/11
switchport mode trunk
switchport trunk allowed vlan 717,817,917

!PUERTO 6 HYBTID A LA ROSETA
interface fa0/12
!si el switch es el 50
switchport mode access 
switchport access vlan 1000


# PARTE MARC #
!PUERTO 1 DEPT A
interface fa0/1
switchport mode access
switchport access vlan 718

!PUERTO 2 DEPT B
interface fa0/2
switchport mode access
switchport access vlan 818

!PUERTO 3 SERVERS
interface fa0/3
switchport mode access
switchport access vlan 918

!PUERTO 4 TRUNK MKT SALIDA INTERNET
interface fa0/4
switchport mode trunk
switchport trunk allowed vlan 1000,1005

!PUERTO 5 TRUNK MKT REDES INTERNAS
interface fa0/5
switchport mode trunk
switchport trunk allowed vlan 718,818,918

# PARTE SERGI #

!PUERTO 1 DEPT A
interface fa0/13
switchport mode access
switchport access vlan 716

!PUERTO 2 DEPT B
interface fa0/14
switchport mode access
switchport access vlan 816

!PUERTO 3 SERVERS
interface fa0/15
switchport mode access
switchport access vlan 916

!PUERTO 4 TRUNK MKT SALIDA INTERNET
interface fa0/16
switchport mode trunk
switchport trunk allowed vlan 1000,1005

!PUERTO 5 TRUNK MKT REDES INTERNAS
interface fa0/17
switchport mode trunk
switchport trunk allowed vlan 716,816,916

# 2. VLAN's y IP's en microtik #

#Añadir VLANs
/interface vlan add vlan-id=717 interface=eth3 name=deptA
/interface vlan add vlan-id=817 interface=eth3 name=deptB
/interface vlan add vlan-id=917 interface=eth3 name=servers
/interface vlan add vlan-id=1000 interface=eth1 name=escola
/interface vlan add vlan-id=1005 interface=eth1 name=isp

#Añadir IPs
/ip address add interface=deptA address=10.27.117.1/24
/ip address add interface=deptB address=10.28.117.1/24
/ip address add interface=servers address=10.29.117.1/24
/ip address add interface=escola address=192.168.3.217/16
/ip address add interface=isp address=10.30.0.117/24


#masquerade y default gateway
/ip firewall nat add action=masquerade chain=srcnat out-interface=escola
/ip route add dst-address=0.0.0.0/0 gateway=192.168.0.1 

# 3. SERVERS DHCP #

# Rango deptA
/ip pool add name=rangodeptA ranges=10.27.117.100-10.27.117.200 
/ip dhcp-server network add dns-server=8.8.8.8 gateway=10.27.117.1 netmask=24 domain=deptA.com address=10.27.117.0/24
/ip dhcp-server add address-pool=rangodeptA interface=deptA

#Rango deptB
/ip pool add name=rangodeptB ranges=10.28.117.100-10.28.117.200 
/ip dhcp-server network add dns-server=8.8.8.8 gateway=10.28.117.1 netmask=24 domain=deptB.com address=10.28.117.0/24
/ip dhcp-server add address-pool=rangodeptB interface=deptB

#Habilitar y cambiar nombre a wlan1 (wifi)
/interface wireless set ssid=mkt17 wlan1 
/interface wireless set wlan1 disabled=no
/ip address add interface=wlan1 address=10.50.117.1/24

#Rango wifi
/ip pool add name=rangowireless ranges=10.50.117.10-10.50.117.250 
/ip dhcp-server network add dns-server=8.8.8.8 gateway=10.50.117.1 netmask=24 domain=wifi.com address=10.50.117.0/24
/ip dhcp-server add address-pool=rangowireless interface=wlan1

#Activamos los 3 dhcp-servers

/ip dhcp-server set 0,1,2 disabled=no

# 4. Definir default gateway por router inf y verificar que salen, cambiar
#default gateway por router-isp y verificar que salen. Dejar la ruta con más 
#peso a router-inf. Verificar que al desconectar router-inf se cambia la salida por
#router-isp # 

# Dar menos preferencia al isp que a escola.
/ip route add dst-address=0.0.0.0/0 gateway=10.30.0.1 distance=2

# 5. Activar las marcas de rutas para que desde la vlan de servers se salga por router-isp y desde el resto de redes se salga por router-inf #

/ip firewall mangle
add action=mark-connection chain=prerouting connection-state=new \
    new-connection-mark=fromservers src-address=10.29.117.0/24
add action=mark-routing chain=prerouting in-interface=servers new-routing-mark=serverstoisp

/ip route add dst-address=0.0.0.0/0 gateway=10.30.0.1 routing-mark=serverstoisp

# 6. Reglas de firewall para evitar accesos desde deptA a deptB y desdewifi solo a server-web y a internet #

# Limitar de deptA a deptB #
/ip firewall filter add chain=forward src-address=10.27.117.0/24 dst-address=10.28.117.0/24 action=drop
# Limitar de deptB a deptA #
/ip firewall filter add chain=forward src-address=10.28.117.0/24 dst-address=10.27.117.0/24 action=drop
#Excepcion depts
/ip firewall filter add chain=forward connection-state=established,related action=accept place-before=0

# Wifi solo a server web y internet #
/ip firewall filter add chain=forward src-address=10.50.117.0/24 action=drop

/ip firewall filter add chain=forward src-address=10.50.117.0/24 dst-address=10.29.117.100 action=accept place-before=1
/ip firewall filter add chain=forward src-address=10.50.117.0/24 out-interface=escola action=accept place-before=1

# 7. Reglas de firewall para que sólo haya telnet desde ips de gestión y servidor ssh por puerto 12322  #

# Dropea todo lo que vaya por telnet
/ip firewall filter add chain=input dst-port=23 protocol=tcp src-address=192.168.88.0/24 action=accept place-before=1
# Menos los de la red de gestion
/ip firewall filter add chain=input dst-port=23 protocol=tcp action=drop place-before=2
# Ssh por el puerto 12322
/ip service set ssh port=12322


ip address set [find interface=isp] interface=escola

# 8. Limitar ancho de banda de bajada en wireless a 500Kbps #

/queue tree
add limit-at=100k max-limit=100k name=cola_wifi packet-mark=no-mark parent=wlan1

# 9. Repartir por igual el tráfico entre todos los usuarios de deptA y deptB #

#Marcamos la conexion de los dos depts#
/ip firewall mangle
add action=mark-connection chain=prerouting connection-state=new \
    new-connection-mark=deptA src-address=10.27.117.0/24
add action=mark-connection chain=prerouting connection-state=new \
    new-connection-mark=deptB src-address=10.28.117.0/24

#Marcamos los paquetes
/ip firewall mangle
add action=mark-packet chain=prerouting connection-mark=deptA \
    new-packet-mark=packetdeptA
add action=mark-packet chain=prerouting connection-mark=deptB \
    new-packet-mark=packetdeptB

/queue tree
# Se puede poner packet-mark=no-mark si no hemos marcado conexiones y packetes
add name=deptA parent=deptA queue=pcq-download-default packet-mark=packetdeptA
add name=deptB parent=deptB queue=pcq-download-default packet-mark=packetdeptB
