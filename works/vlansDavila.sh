#!/bin/bash

IFNAME="enp3s0"
PREFIX="loko"
NUMHOST=17

for i in {1..9}
do
  ip link add link $IFNAME name $PREFIX$i type vlan id $i
  ip a a 172.16.20$i.$NUMHOST/24 dev $PREFIX$i
  ip link set $PREFIX$i up
done

