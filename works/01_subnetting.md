# Exercicis de Subxarxes

##### Instruccions generals:
1. Alerta, feu tots els càlculs sense calculadora!
2. Si necessiteu fer càlculs en un full apart, si us plau, indiqueu-m'ho en cada exercici i entregueu-me aquest full.
3. En total hi ha 5 exercicis (reviseu totes les pàgines)

##### 1. Donades les següents adreces IP, completa la següent taula (indica les adreces de xarxa i de broadcast tant en format decimal com en binari).


IP | Adreça de Xarxa | Adreça de Broadcast Extern | Rang IPs per a Dispositius 
---|---|---|---
172.16.4.23/25 | 172.16.4.0 | 172.16.4.127 | .1 a .126
174.187.55.6/23 | 174.187.54.0 | 174.187.55.255 | .54.1 a .55.254
10.0.25.253/18 | 10.0.0.0 | 10.0.63.255 | .0.1 a .63.254
209.165.201.30/27 | 209.165.201.0 | 209.165.201.31 | .1 a .30


##### 2. Donada la xarxa 172.28.0.0/16, calcula la màscara de xarxa que necessites per poder fer 80 subxarxes. Tingues en compte que cada subxarxa ha de tenir capacitat per a 400 dispositius. Indica la màscara de xarxa calculada tant en format decimal com en binari.

Com a minim animerem a un /23 --> 11111111.11111111.11111110.00000000 --> 255.255.254.0

32 - 23 = 9 bits per host
2⁹ = 512 combinacions posibles
512 - 2 adreçes reservades = 510 hosts


##### 3. Donada la xarxa 10.192.0.0
a) Calcula la màscara de xarxa que necessites per poder adreçar 1500 dispositius. Indica-la tant en format decimal com en binari.

- 32 - 21 = 11, per tant, 11 zeros.
- 22¹¹ = 2048 combinacions possibles. 1500 és més petit que 2048 per tant ens val.
- 2048 - xarxa - broadcast = 2046 hosts
- Per tant, mascara sera amb 11 zeros:
    - 11111111.11111111.11111000.00000000
    - 255.255.248.0
    - 10.192.0.0/21

b) Si fixem l'adreça de la xarxa mare a la 10.192.0.0/10, quantes subxarxes de 1500 dispositius podrem fer?

- 32-10 = 22 zeros -> 11111111.11000000.00000000.00000000
- 22 - 11 zeros de supernet = 11 
- Posibilitats amb 11 xifres de binari = 2¹¹ = 2048 subxarxes de 1500 dispositius.(hi caben realment 2048)

c) Si ara considerem que l'adreça de xarxa mare és la 10.192.0.0 amb la màscara de xarxa calculada a l'apartat (a), calcula la nova màscara de xarxa que permeti fer 3 subxarxes de 500 dispositius cadascuna. Indica-la tant en format decimal com en binari.

- Abans: 10.192.0.0/21
- 32 - 23 = 9 bits per hosts, per tant, 9 zeros.
- 2⁹ = 512 combinacions possibles (ja que 2⁸ = 256 i no arriba.)
- 512 - xarxa - broadcast = 510 hosts (510 > 500)
- Per tant, mascara serà amb 9 zeros:
    - 11111111.11111111.11111110.00000000 (format binari)
    - 255.255.254.0 (format decimal)
    - 10.192.0.0/23


##### 4. Donada la xarxa 10.128.0.0 i sabent que s'ha d'adreçar un total de 3250 dispositius
a) Calcula la màscara de xarxa per crear una adreça de xarxa mare que permeti adreçar tots els dispositius indicats. Indica-la tant en format decimal com en binari.

Una màscara ( /20 255.255.240.0 = 20    11111111.11111111.1111 0000.00000000 ) = 4094 dispositius

b) Calcula la nova màscara de xarxa que permeti fer 5 subxarxes amb 650 dispositius cadascuna d'elles. Indica-la tant en format decimal com en binari.

2³ = 8 subxarxes minim --> ( /20 a /23)  255.255.252.0 = 22    11111111.11111111.111111 00.00000000
(ja que 2² = 4 subxarxes i necesitem 5)

c) Comprova que, realment, cada subxarxa pot adreçar els 650 dispositius demanats. En cas que no sigui així, quina és la capacitat màxima de dispositius de cadascuna d'aquestes xarxes? Indica la fórmula que fas anar per calcular aquesta dada.

Total hosts / numero subxarxes = hosts per subxarxa
4080 / 8 = 510 hosts per subxarxa. No permet 650 dispositius per xarxa.

d) A partir de la xarxa mare que has obtingut amb la màscara de xarxa calculada a l'apartat (a), podem fer dues subxarxes amb 1625 dispositius cadascuna d'elles? Indica els càlculs que necessites fer per raonar la teva resposta.
Si, pasem a /21 i:
	255.255.248.0 = 21    11111111.11111111.11111 000.00000000
Ens fa 2 subxarxes
	i. Xarxa:   		10.128.0.0/21	00001010.10000000.00000 000.00000000 
	   Broadcast: 		10.128.7.255    00001010.10000000.00000 111.11111111
	   1erHost:   		10.128.0.1      00001010.10000000.00000 000.00000001
	   últimHost:   		10.128.7.254    00001010.10000000.00000 111.11111110
	   Hosts per subxarxa: 	2046

       ii. Xarxa:		10.128.8.0/21   00001010.10000000.00001 000.00000000
	   Broadcast: 		10.128.15.255   00001010.10000000.00001 111.11111111
	   1erHost:   		10.128.8.1    	00001010.10000000.00001 000.00000001
	   últimHost:  		10.128.15.254   00001010.10000000.00001 111.11111110
	   Hosts per subxarxa: 	2046 


5. Donada l'adreça de xarxa mare (AX mare) 172.16.0.0/12, s'han de calcular 6 subxarxes.
a) Quants bits necessites per ampliar la Màscara de Xarxa (MX) per tal de poder fer aquestes 6 subxarxes?

3 bits (Pasem de /12 a /15

b) Dóna la nova MX, tant en format decimal com en binari.

172.16.0.0/15	-->	11111111.11111110.00000000.00000000	-->	255.254.0.0

c) Per cada subxarxa nova que has de crear, indica
i. L'adreça de xarxa, en format decimal i en binari

Xarxa 1 == 10101100.00010000.00000000.00000000 	-->	172.16.0.0
Xarxa 2 == 10101100.00010010.00000000.00000000	-->	172.18.0.0
Xarxa 3 == 10101100.00010110.00000000.00000000	-->	172.20.0.0
Xarxa 4 == 10101100.00011000.00000000.00000000	-->	172.22.0.0
Xarxa 5 == 10101100.00011010.00000000.00000000	-->	172.24.0.0
Xarxa 6 == 10101100.00011100.00000000.00000000	-->	172.26.0.0

ii. L'adreça de broadcast extern, en format decimal i en binari

Xarxa 1 == 10101100.00010001.11111111.11111111	-->	172.17.255.255
Xarxa 2 == 10101100.00010011.11111111.11111111	-->	172.19.255.255
Xarxa 3 == 10101100.00010111.11111111.11111111  -->	172.21.255.255
Xarxa 4 == 10101100.00011001.11111111.11111111	-->	172.23.255.255
Xarxa 5 == 10101100.00011011.11111111.11111111	-->	172.25.255.255
Xarxa 6 == 10101100.00011101.11111111.11111111	-->	172.27.255.255

iii. El rang d'IPs per a dispositius

Xarxa 1 == 172.16.0.1 	-->	172.17.255.254
Xarxa 2 == 172.18.0.1 	-->	172.19.255.254
Xarxa 3 == 172.20.0.1 	--> 	172.21.255.254
Xarxa 4 == 172.22.0.1 	-->	172.23.255.254
Xarxa 5 == 172.24.0.1	-->	172.25.255.254
Xarxa 6 == 172.26.0.1	-->	172.27.255.254


d) Tenint en compte el número de bits que has indicat en l'apartat a), quantes subxarxes podríem fer, realment? Dóna'n la fórmula que s'utilitza per calcular aquesta dada.

8 subxarxes -> 2³ = 8

e) Segons la MX que has calculat als apartats a) i b), quants dispositius pot tenir cada subxarxa? Dóna'n la fórmula que s'utilitza per calcular aquesta dada.

255.254.0.0 	11111111.1111 111 0.00000000.00000000

Cada subxarxa pot tenir 2¹⁷ zeros - xarxa - broadcast = 131070 hosts.

##### 6. Donada l'adreça de xarxa mare (AX mare) 192.168.1.0/24, s'han de calcular 4 subxarxes.

a) Quants bits necessites per ampliar la Màscara de Xarxa (MX) per tal de poder fer aquestes 6 subxarxes?

2 bits més. de /24 a /26

b) Dóna la nova MX, tant en format decimal com en binari.

11111111.11111111.11111111.11000000 --> 255.255.255.192

c) Per cada subxarxa nova que has de crear, indica
i. L'adreça de xarxa, en format decimal i en binari

11000000.10101000.00000001.00000000 --> 192.168.1.0
11000000.10101000.00000001.01000000 --> 192.168.1.64
11000000.10101000.00000001.10000000 --> 192.168.1.128
11000000.10101000.00000001.11000000 --> 192.168.1.192

ii. L'adreça de broadcast extern, en format decimal i en binari

11000000.10101000.00000001.00111111 --> 192.168.1.63
11000000.10101000.00000001.01111111 --> 192.168.1.127
11000000.10101000.00000001.10111111 --> 192.168.1.191
11000000.10101000.00000001.11111111 --> 192.168.1.255

iii. El rang d'IPs per a dispositius

Xarxa 1 == 192.168.1.1 		--> 192.168.1.62
Xarxa 2 == 192.168.1.65 	--> 192.168.1.126
Xarxa 3 == 192.168.1.129 	--> 192.168.1.190
Xarxa 4 == 192.168.1.193	--> 192.168.1.254


d) Tenint en compte el número de bits que has indicat en l'apartat a), podríem fer més de 4 subxarxes? Dóna'n la fórmula que has utilitzat per respondre a la pregunta.

No, perque hem fet 2² = 4 subxarxes.

e) Segons la MX que has calculat als apartats a) i b), quants dispositius pot tenir cada subxarxa? Dóna'n la fórmula que s'utilitza per calcular aquesta dada.

2⁶ == 64 - xarxa - broadcast = 62 dispositius per subxarxa
