#### 1. ip a

Borra todas las rutas y direcciones ip, para el servicio NetworkManager y asegúrate que no queda ningún demonio de dhclient corriendo. Comprueba que no queda ninguna con "ip a" y "ip r"
	
	ip r f all
	ip a f dev enp3s0
	ip a
	2: enp3s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 74:d0:2b:c9:f7:ac brd ff:ff:ff:ff:ff:ff
	ip r (sense sortida)
	

Ponte las siguientes ips en tu tarjeta ethernet: 
2.2.2.2/24, 
3.3.3.3/16, 
4.4.4.4/25

	ip a a 2.2.2.2/24 dev enp3s0
	ip a a 3.3.3.3/16 dev enp3s0
	ip a a 4.4.4.4/25 dev enp3s0


Consulta la tabla de rutas de tu equipo

	# ip r
	2.2.2.0/24 dev enp3s0  proto kernel  scope link  src 2.2.2.2 
	3.3.0.0/16 dev enp3s0  proto kernel  scope link  src 3.3.3.3 
	4.4.4.0/25 dev enp3s0  proto kernel  scope link  src 4.4.4.4 


Haz ping a las siguientes direcciones y justifica por qué en algunas sale el mensaje de "Network is unrecheable", en otras contesta y en otras se queda esperando sin dar mensajes de error:

2.2.2.2 , 2.2.2.254 , 2.2.5.2 , 3.3.3.35 , 3.3.200.45 , 4.4.4.8, 4.4.4.132

	ping 2.2.2.2		--> OK								   --> És la nostra ip
	ping 2.2.2.254		--> Destination Host Unreachable       --> No hi ha ningú amb aquesta ip o la maquina esta apagada
	ping 2.2.5.2		--> Network is unreachable             --> Perque no estem a la mateixa xarxa
	ping 3.3.3.35		--> Destination Host Unreachable       --> No hi ha ningú amb aquesta ip o la maquina esta apagada
	ping 3.3.200.45		--> Destination Host Unreachable       --> No hi ha ningú amb aquesta ip o la maquina esta apagada
	ping 4.4.4.8		--> Destination Host Unreachable       --> No hi ha ningú amb aquesta ip o la maquina esta apagada
	ping 4.4.4.132		--> Network is unreachable             --> Perque no estem a la mateixa xarxa

#### 2. ip link

Borra todas las rutas y direcciones ip de la tarjeta ethernet

	ip a f dev enp3s0
	ip a
	2: enp3s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 74:d0:2b:c9:f7:ac brd ff:ff:ff:ff:ff:ff

Conecta una segunda interfaz de red por el puerto usb
	
	ip a
	4: usb0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
    link/ether 00:e0:4c:36:1a:b2 brd ff:ff:ff:ff:ff:ff

Cambiale el nombre a usbdelchi
	
	# ip link set usb0 down
	# ip link set usb0 name usbdelchi
	# ip link set usbdelchi up

Modifica la dirección MAC

	# ip link set usbdelchi down
	# ip link set usbdelchi address 00:11:22:33:44:55
	# ip link set usbdelchi up


Asígnale la direcció ip 5.5.5.5/24 a usbdelchi y 7.7.7.7/24 a la tarjeta de la placa base.

	# ip a a 5.5.5.5/24 dev usbdelchi
	# ip a a 7.7.7.7/24 dev enp3s0

Observa la tabla de rutas

	# ip r
	5.5.5.0/24 dev usbdelchi  proto kernel  scope link  src 5.5.5.5 linkdown 
	7.7.7.0/24 dev enp3s0  proto kernel  scope link  src 7.7.7.7

#### 3. iperf

Borra todas las rutas y direcciones ip de la tarjeta ethernet

	# ip a f dev enp3s0

En cada ordenador os ponéis la ip 172.16.99.XX/24 (XX=puesto de trabajo)

	# ip a a 172.16.99.17/24 dev enp3s0

Lanzar iperf en modo servidor en cada ordenador

	# iperf -s
	------------------------------------------------------------
	Server listening on TCP port 5001
	TCP window size: 85.3 KByte (default)

Comprueba con netstat en qué puerto escucha

	# netstat -utlnp
	Active Internet connections (only servers)
	Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name    
	tcp        0      0 0.0.0.0:5001            0.0.0.0:*               LISTEN      5267/iperf   

Conectarse desde otro pc como cliente

	# iperf -c 172.16.99.18
	------------------------------------------------------------
	Client connecting to 172.16.99.18, TCP port 5001
	TCP window size: 85.0 KByte (default)
	------------------------------------------------------------
	[  3] local 172.16.99.17 port 36882 connected with 172.16.99.18 port 5001

	# iperf -s
	------------------------------------------------------------
	Server listening on TCP port 5001
	TCP window size: 85.3 KByte (default)
	------------------------------------------------------------
	[  4] local 172.16.99.17 port 5001 connected with 172.16.99.18 port 46598

Repetir el procedimiento y capturar los 30 primeros paquetes con tshark

	tshark -i enp3s0 -c30 "port 5001"

Encontrar los 3 paquetes del handshake de tcp
	
	# tshark -i enp3s0 -c3 "port 5001"
	Running as user "root" and group "root". This could be dangerous.
	Capturing on 'enp3s0'
	1 0.000000000 172.16.99.18 → 172.16.99.17 TCP 74 46600→5001 [SYN] Seq=0 Win=29200 Len=0 MSS=1460 SACK_PERM=1 TSval=13146651 TSecr=0 WS=128
	2 0.000032050 172.16.99.17 → 172.16.99.18 TCP 74 5001→46600 [SYN, ACK] Seq=0 Ack=1 Win=28960 Len=0 MSS=1460 SACK_PERM=1 TSval=12827417 TSecr=13146651 WS=128
	3 0.000116573 172.16.99.18 → 172.16.99.17 TCP 66 46600→5001 [ACK] Seq=1 Ack=1 Win=29312 Len=0 TSval=13146651 TSecr=12827417
	3 packets captured

Abrir dos servidores en dos puertos distintos

	# iperf -s -p 5001
	# iperf -s -p 5002

Observar como quedan esos puertos abiertos con netstat

	# netstat -utlnp
	Active Internet connections (only servers)
	Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name    
	tcp        0      0 0.0.0.0:5001            0.0.0.0:*               LISTEN      5581/iperf          
	tcp        0      0 0.0.0.0:5002            0.0.0.0:*               LISTEN      5578/iperf 

	
Conectarse al servidor con dos clientes y que la prueba dure 1 minuto

[server]
	# iperf -s 
	------------------------------------------------------------
	Server listening on TCP port 5001
	TCP window size: 85.3 KByte (default)
	------------------------------------------------------------
	[  5] local 172.16.99.17 port 5001 connected with 172.16.99.17 port 38818
	[  6] local 172.16.99.17 port 5001 connected with 172.16.99.17 port 38820
	[ ID] Interval       Transfer     Bandwidth
	[  5]  0.0-60.0 sec   125 GBytes  17.9 Gbits/sec
	[  6]  0.0-65.3 sec   125 GBytes  16.4 Gbits/sec

[cliente 1]
	# iperf -t 60 -c 172.16.99.17
	------------------------------------------------------------
	Client connecting to 172.16.99.17, TCP port 5001
	TCP window size: 2.50 MByte (default)
	------------------------------------------------------------
	[  3] local 172.16.99.17 port 38820 connected with 172.16.99.17 port 5001
	[ ID] Interval       Transfer     Bandwidth
	[  3]  0.0-60.0 sec   125 GBytes  17.9 Gbits/sec

[cliente 2]
	# iperf -t 60 -c 172.16.99.17
	------------------------------------------------------------
	Client connecting to 172.16.99.17, TCP port 5001
	TCP window size: 2.50 MByte (default)
	------------------------------------------------------------
	[  3] local 172.16.99.17 port 38818 connected with 172.16.99.17 port 5001
	[ ID] Interval       Transfer     Bandwidth
	[  3]  0.0-60.0 sec   125 GBytes  17.9 Gbits/sec

Mientras tanto con netstat mirar conexiones abiertas
	
	# netstat -utlnp

