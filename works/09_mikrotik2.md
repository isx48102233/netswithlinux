EJERCICIO MIKROTIK 2.

OBJETIVO: CREAR DOS REDES WIFIS CON DISTINTOS TIPOS DE SEGURIDAD


 MIKROTIK
 +-------------------+
 |  1  2  3  4  wifi |
 +-------------------+
    |  |  |  |
    |  |  |  |
    |  |  |  +
    |  |  |  Conexión a un switch  con vlans
    |  |  +
    |  |  Interface de red placa base
    |  +
    |  Interface USB
    |  Ip 192.168.88.1XX/24
    +
    Punto de red del aula
    para salida a internet

## Pasos para conseguir tener 2 redes wifis separadas con distintos
niveles de seguridad.

Recordamos que:
- Solo disponemos de una interface wifi que trabaja en la banda de 2,4GHz.
- Este modelo de mikrotik dispone de 4 interfaces ethernet y 1 interface wifi
- En el PC dispondremos de una interface USB que es la que usaremos para
configurar el router por el puerto 2

Para conseguir dos redes wifi a partir de una sola interface hay que crear
sub-interfaces o interfaces virtuales. Cada interface virtual ha de estar 
asociada a un id de vlan (número de 1 a 2048)

Si queremos comunicarnos en la misma red entre un dispositivo wifi y un dispositivo
cableado hay que crear bridges entre la interface wifi y la interface ethernet.

### Eliminar configuración inicial
La configuración inicial que viene con la mikrotik podemos consultarla haciendo 
/export y obtenmos:

```
/interface bridge
add admin-mac=6C:3B:6B:30:B8:4F auto-mac=no comment=defconf name=bridge
/interface wireless
set [ find default-name=wlan1 ] band=2ghz-b/g/n channel-width=20/40mhz-Ce \
    disabled=no distance=indoors frequency=auto mode=ap-bridge ssid=\
    MikroTik-30B852 wireless-protocol=802.11
/interface ethernet
set [ find default-name=ether2 ] name=ether2-master
set [ find default-name=ether3 ] master-port=ether2-master
set [ find default-name=ether4 ] master-port=ether2-master
/ip neighbor discovery
set ether1 discover=no
set bridge comment=defconf
/ip pool
add name=default-dhcp ranges=192.168.88.10-192.168.88.254
/ip dhcp-server
add address-pool=default-dhcp disabled=no interface=bridge name=defconf
/interface bridge port
add bridge=bridge comment=defconf interface=ether2-master
add bridge=bridge comment=defconf interface=wlan1
/ip address
add address=192.168.88.1/24 comment=defconf interface=bridge network=\
    192.168.88.0
/ip dhcp-client
add comment=defconf dhcp-options=hostname,clientid disabled=no interface=ether1
/ip dhcp-server network
add address=192.168.88.0/24 comment=defconf gateway=192.168.88.1
/ip dns
set allow-remote-requests=yes
/ip dns static
add address=192.168.88.1 name=router
/ip firewall filter
add chain=input comment="defconf: accept ICMP" protocol=icmp
add chain=input comment="defconf: accept established,related" connection-state=\
    established,related
add action=drop chain=input comment="defconf: drop all from WAN" in-interface=\
    ether1
add action=fasttrack-connection chain=forward comment="defconf: fasttrack" \
    connection-state=established,related
add chain=forward comment="defconf: accept established,related" \
    connection-state=established,related
add action=drop chain=forward comment="defconf: drop invalid" connection-state=\
    invalid
add action=drop chain=forward comment="defconf:  drop all from WAN not DSTNATed" \
    connection-nat-state=!dstnat connection-state=new in-interface=ether1
/system routerboard settings
set boot-device=flash-boot cpu-frequency=650MHz protected-routerboot=disabled
/tool mac-server
set [ find default=yes ] disabled=yes
add interface=bridge
/tool mac-server mac-winbox
set [ find default=yes ] disabled=yes

```

A continuación modificamos todas esas partes de la configuración que no nos
interesan para dejar la mikrotik pelada, sin configuraciones iniciales
que interfieran con nuestra nueva programación del router:

```
# Cambiamos dirección IP para que la gestione directamente la interface 2
ip address set 0 interface=ether2-master

# Eliminamos el bridge

/interface bridge port remove 1
/interface bridge port remove 0
/interface bridge remove 0  

# Eliminamos que un puerto sea master de otro

/interface ethernet set [ find default-name=ether3 ] master-port=none
/interface ethernet set [ find default-name=ether4 ] master-port=none

# Cambiamos nombres a los puertos

/interface ethernet set [ find default-name=ether1 ] name=eth1
/interface ethernet set [ find default-name=ether2 ] name=eth2
/interface ethernet set [ find default-name=ether3 ] name=eth3
/interface ethernet set [ find default-name=ether4 ] name=eth4

# Deshabilitamos la wifi

/interface wireless set [ find default-name=wlan1 ] disabled=yes

# Eliminamos servidor y cliente dhcp

/ip pool remove 0
/ip dhcp-server network remove 0
/ip dhcp-server remove 0
/ip dhcp-client remove 0
/ip dns static remove 0
/ip dns set allow-remote-requests=no

# Eliminamos regla de nat masquerade
/ip firewall nat remove 0

```

Ahora le cambiamos el nombre y guardamos la configuración inicial por
si la necesitamos restaurar en cualquier momento

```
/system> identity set name=infMKT17Adri
/system> backup save name="20170316_zeroconf"
```

###[ejercicio1] Explica el procedimiento para resetear el router y restaurar
este backup, verifica con /export que no queda ninguna configuración inicial 
que pueda molestarnos para empezar a programar el router

Para resetear el router hacemos ```/system reset-configuration``` o directamente le damos al boton RES/WPS apretando unos cuantos segundos.
Para cargar el backup:
```
/system backup load name="20170316_zeroconf"
```

### Crear interfaces y bridges

Primero hay que pensar en capa 2, vamos a crear un bridge por cada red wifi
que tenga un punto de red cableado asociado a una vlan

Trabajaremos con dos vlans:

*117: red pública
*217: red privada

Hay que crear unas interfaces virtuales wifi adicional y cambiar los ssids
para reconocerlas cuando escaneemos las wifis

```
/interface wireless set 0 name="wFree"
/interface wireless set 0 ssid="free117"
/interface wireless add name="wPrivate" ssid="private217" master-interface=wFree
```
Cambiamos nombre y creamos una nueva wifi

###[ejercicio2] Explica por qué aparecen las interfaces wifi como disabled
al hacer /interface print. Habilita y deshabilita estar interfaces. Cambiales
el nombre a wFree wPrivate y dejalas deshabilitadas

Al configurar hemos puesto que la wifi estuviese deshabilitada. 
A más, al crear una nueva interfaz aparecen como disabled

## Crear interfaces virtuales y hacer bridges

```
/interface vlan add name eth4-vlan117 vlan-id=117 interface=eth4
/interface vlan add name eth4-vlan217 vlan-id=217 interface=eth4   

/interface bridge add name=br-vlan117
/interface bridge add name=br-vlan217

/interface bridge port add interface=eth4-vlan117 bridge=br-vlan117
/interface bridge port add interface=eth3 bridge=br-vlan117
/interface bridge port add interface=wFree  bridge=br-vlan117    

/interface bridge port add interface=eth4-vlan217 bridge=br-vlan217
/interface bridge port add interface=wPrivate bridge=br-vlan217

/interface print

```


### [ejercicio3] Comenta cada una de estas líneas de la configuración
anterior para que quede bien documentado

#Añadimos 2 vlans con nombre "vlan117" y "vlan217" con id "117" y "217" en la eth4
```
/interface vlan add name eth4-vlan117 vlan-id=117 interface=eth4
/interface vlan add name eth4-vlan217 vlan-id=217 interface=eth4   
```
#Añadimos 2 puentes con nombre "br-vlan117" y "br-vlan217"
```
/interface bridge add name=br-vlan117
/interface bridge add name=br-vlan217
```
#Conectamos ----- al bridge "br-vlan117"
```
/interface bridge port add interface=eth4-vlan117 bridge=br-vlan117
/interface bridge port add interface=eth3 bridge=br-vlan117
/interface bridge port add interface=wFree bridge=br-vlan117    
```
#Conectamos ----- al bridge "br-vlan217"
```
/interface bridge port add interface=eth4-vlan217 bridge=br-vlan217
/interface bridge port add interface=wPrivate   bridge=br-vlan217
```
#Vemos que todo ha ido correctamente
```
/interface print
```

### [ejercicio4] Pon una ip 172.17.117.1/24 a br-vlan117 
y 172.17.217.1/24 a br-vlan217 y verifica desde la interface de la placa 
base de tu ordenador que hay conectividad (puedes hacer ping) configurando
unas ips cableadas. Cuando lo hayas conseguido haz un backup de la configuración
#En el router añadimos:

```
ip address add address=172.17.117.1/24 interface="br-vlan117"
ip address add address=172.17.217.1/24 interface="br-vlan217"  
```
#Y en la placa:
```
ip a a 172.17.117.50/24 dev enp3s0
ping 172.17.117.1
```
PING 172.17.117.1 (172.17.117.1) 56(84) bytes of data.
64 bytes from 172.17.117.1: icmp_seq=1 ttl=64 time=0.393 ms
64 bytes from 172.17.117.1: icmp_seq=2 ttl=64 time=0.197 ms

#para la eth4 hemos de crear una vlan en fedora para comprobar:
```
ip link add link enp3s0 name vlanlinux type vlan id 217
ip a a 172.17.217.50/24 dev vlanlinux
ip link set vlanlinux up
ping 172.17.217.1
```
PING 172.17.217.1 (172.17.217.1) 56(84) bytes of data.
64 bytes from 172.17.217.1: icmp_seq=1 ttl=64 time=0.430 ms
64 bytes from 172.17.217.1: icmp_seq=2 ttl=64 time=0.229 ms

#Hacemos backup
```
/system> backup save name="20170321_ej4ok"
```
### [ejercicio5] Crea una servidor dhcp para cada una de las redes en los rangos
.101 a .250 de las respectivas redes. Asignar ip manual a la eth1. 
Crear reglas de nat para salir a internet. 

#Añadimos los rangos de ip's.
```
ip pool add ranges=172.17.117.100-172.17.117.200 name=rang_public
ip pool add ranges=172.17.217.100-172.17.217.200 name=rang_private 
```

#Damos los rangos de ip a cada interfaz(bridge)
```
ip dhcp-server add address-pool=rang_public 
interface: br-vlan117
ip dhcp-server add address-pool=rang_private
interface: br-vlan217
```
#Activamos el dhcp-server
```
ip dhcp-server set 0 disabled=no     
```
#Añadimos el red
```
ip dhcp-server network add dns-server=8.8.8.8 gateway=172.17.117.1 netmask=24 domain=MTK17.com address=172.17.117.0/24
ip dhcp-server network add dns-server=8.8.8.8 gateway=172.17.217.1 netmask=24 domain=MTK17.com address=172.17.217.0/24  
```
#Activamos los dhcp de los bridges
```
ip dhcp-server set 0,1  disabled=no
```
#
```
interface wireless set ssid=free117
numbers: 0
interface wireless set ssid=private117    
numbers: 1
```
#NAT#
```
/ip address add address=192.168.3.217/16 interface=eth1  
/ip firewall nat add out-interface=eth1 action=masquerade chain=srcnat
/ip route add dst-address=0.0.0.0/0 gateway=192.168.0.1
```

### [ejercicio6] Activar redes wifi y dar seguridad wpa2
```
interface wireless set 0,1 disabled=no
```
```
# Creamos un nuevo perfil al que cambiaremos la contraseña y assignaremos a la red privada, le ponemos el tipo de seguridad y lo activamos. en este orden
```
interface wireless security-profile add name=profileprivate     

interface wireless security-profiles edit 1 wpa2-pre-shared-key **ContraPrivada**

interface wireless set security-profile=profileprivate 
numbers: 1
interface wireless security-profiles> set 1 authentication-types=wpa2-psk 

interface wireless security-profiles> set 1 mode=static-keys-required 
```

## SECURITY ##
## Cambiamos el puerto donde escucha el ssh
```
/ip service
set ssh port=22340
```
## Permitimos que desde cualquier ip se puedan conectar a los puertos
del router que queramos
```
/ip firewall filter
add chain=input comment="defconf: accept ssh on port 22340" \
    dst-port=22340 protocol=tcp 
```
## Si quiero añadir esa regla la primera de todas si ya disponemos
 de otras reglas usamos place-before
```
/ip firewall filter
add chain=input comment="defconf: accept ssh on port 22340" \
    dst-port=22340 protocol=tcp place-before=0
```
## Cambiamos el password (si no tiene no hace falta poner old-password)
```
/password new-password=clase confirm-new-password=clase
```
## Sincronizamos el reloj del router usando el protocolo ntp a 
partir de alguna ip de los servidores de tiempo. En un linux:
```
/system ntp client
set enabled=yes primary-ntp=213.251.52.234 secondary-ntp=158.227.98.15
```
### [ejercicio6b] Opcional (monar portal cautivo)

### [ejercicio7] Firewall para evitar comunicaciones entre las redes Free y Private. Limitar
puertos en Free.
```
add action=drop chain=forward comment="defconf: DROP ICMP" dst-address=\
    172.17.217.0/24 src-address=172.17.117.0/24
```

### [ejercicio8] Conexión a switch cisco con vlans

### [ejercicio8b] (Opcional) Montar punto de acceso adicional con un AP de otro fabricante

### [ejercicio9] QoS

### [ejercicio9] Balanceo de salida a internet

### [ejercicio10] Red de servidores con vlan de servidores y redirección de puertos

### [ejercicio11] Firewall avanzado

```
/ip firewall filter add chain=input action=accept protocol=tcp dst-port=22 in-interface=eth1 place-before=0

```
