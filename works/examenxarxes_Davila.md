# 1. Configurar Switch #
no spanning-tree vlan 2-1000

!PUERTO 1 ROSETA
interface fa0/1
!si el switch es el 50
switchport mode access 
switchport access vlan 80

!PUERTO 2 MKT_INTERNET
interface fa0/2
switchport mode trunk
switchport trunk allowed vlan 80

!PUERTO 3 MKT-XARXES_INTERNES
interface fa0/3
switchport mode trunk
switchport trunk allowed vlan 375,475,775

!PUERTO 4 deptA
interface fa0/4
switchport mode access
switchport access vlan 375

!PUERTO 5 deptB
interface fa0/5
switchport mode access
switchport access vlan 475

!PUERTO 6 servers
interface fa0/6
switchport mode access
switchport access vlan 775

# 2. VLAN's y IP's en microtik #

#Añadir VLANs
/interface vlan add vlan-id=375 interface=eth4 name=deptA
/interface vlan add vlan-id=475 interface=eth4 name=deptB
/interface vlan add vlan-id=775 interface=eth4 name=servers
/interface vlan add vlan-id=80 interface=eth3 name=inet

#Añadir IPs
/ip address add interface=deptA address=10.33.175.1/24
/ip address add interface=deptB address=10.34.175.1/24
/ip address add interface=servers address=10.37.175.1/24
/ip address add interface=inet address=192.168.3.175/16
/ip address add interface=inet address=10.30.0.175/24

#masquerade y default gateway
/ip firewall nat add action=masquerade chain=srcnat out-interface=inet
/ip route add dst-address=0.0.0.0/0 gateway=192.168.0.1

# 3. Configurar servers DHCP #


# Rango deptA
/ip pool add name=rangodeptA ranges=10.33.175.10-10.33.175.100
/ip dhcp-server network add dns-server=8.8.8.8 gateway=10.33.175.1 netmask=24 domain=deptA.com address=10.33.175.0/24
/ip dhcp-server add address-pool=rangodeptA interface=deptA

#Rango deptB
/ip pool add name=rangodeptB ranges=10.34.175.10-10.34.175.100
/ip dhcp-server network add dns-server=8.8.8.8 gateway=10.34.175.1 netmask=24 domain=deptB.com address=10.34.175.0/24
/ip dhcp-server add address-pool=rangodeptB interface=deptB

#Habilitar y cambiar ssid a wlan1 (wifi)
/interface wireless set ssid=mkt17 wlan1 
/interface wireless set wlan1 disabled=no
/ip address add interface=wlan1 address=172.17.50.1/23

#Rango wifi
/ip pool add name=rangowireless ranges=172.17.50.10-172.17.51.250
/ip dhcp-server network add dns-server=8.8.8.8 gateway=172.17.50.1 netmask=23 domain=wifi.com address=172.17.50.0/23
/ip dhcp-server add address-pool=rangowireless interface=wlan1

#Activamos los 3 dhcp-servers

/ip dhcp-server set 0,1,2 disabled=no

# COMPROVACIONES #
#deptA
#dhclient -r enp3s0
#dhclient enp3s0
#te conectas a google y funciona

#deptB
#dhclient -r enp3s0
#dhclient enp3s0
#te conectas a google y funciona

#servers
#dhclient -r enp3s0
#dhclient -r enp3s0 
#ip a a 10.37.175.100/24 dev enp3s0
#ip r a default via 10.37.175.1
#te conectas a google y funciona

#wifi 
#conectas la wifi y cargas un página y debe funcionar

# 4. default gateway inetB y dar ruta con mas peso a inetA # 
/ip route add dst-address=0.0.0.0/0 gateway=10.30.0.1 distance=2
# 5. Marcas #

/ip firewall mangle
#Marcamos la conexion #
add action=mark-connection chain=prerouting connection-state=new \
    new-connection-mark=fromservers src-address=10.37.175.0/24
#Marcamos el routing
add action=mark-routing chain=prerouting in-interface=servers new-routing-mark=serverstoinetB
#Usamos la marca de routing para enviar servers por inetB
/ip route add dst-address=0.0.0.0/0 gateway=10.30.0.1 routing-mark=serverstoinetB

# 6. Reglas de firewall para evitar accesos desde deptA a deptB y desdewifi solo a server-web y a internet #

#Excepcion depts y se pone place-before para saltarse limiticiones
/ip firewall filter add chain=forward connection-state=established,related action=accept place-before=0

# Limitar de deptA a deptB #
/ip firewall filter add chain=forward src-address=10.33.175.0/24 dst-address=10.34.175.0/24 action=reject packet-mark=no-mark place-before=1
# Limitar de deptB a deptA #
/ip firewall filter add chain=forward src-address=10.34.175.0/24 dst-address=10.33.175.0/24 action=reject packet-mark=no-mark place-before=1

# Wifi solo a server web y internet #
#Todo lo que venga de wifi reject
/ip firewall filter add chain=forward src-address=172.17.50.0/23 action=reject
#Añadimos que pueda ir a server web y a internet y lo ponemos con place-before para que no le afecte el reject
/ip firewall filter add chain=forward src-address=172.17.50.0/23 dst-address=10.37.175.200 action=accept place-before=1
/ip firewall filter add chain=forward src-address=172.17.50.0/23 out-interface=inet action=accept place-before=1

# 7. Reglas de firewall para que sólo haya telnet desde ips de gestión y servidor ssh por puerto 5322 y winbox 5323

# Menos los de la red de gestion
/ip firewall filter add chain=input dst-port=23 protocol=tcp action=accept place-before=1
# Dropea todo lo que vaya por telnet
/ip firewall filter add chain=input dst-port=23 protocol=tcp src-address=192.168.88.0/24 action=reject place-before=2
# Ssh por el puerto 5322
/ip service set ssh port=5322
# winbox por el puerto 5323
/ip service set winbox port=5323

# EXPORT DE IP FIREWALL FILTER #

[admin@infMKT17Adri] > ip firewall filter print  
Flags: X - disabled, I - invalid, D - dynamic 
 0    chain=forward action=accept connection-state=established,related log=no log-prefix="" 

 1    chain=forward action=reject reject-with=icmp-network-unreachable src-address=10.33.175.0/24 dst-address=10.34.175.0/24 
      packet-mark=no-mark log=no log-prefix="" 

 2    chain=forward action=reject reject-with=icmp-network-unreachable src-address=10.34.175.0/24 dst-address=10.33.175.0/24 
      packet-mark=no-mark log=no log-prefix="" 

 3    chain=forward action=accept src-address=172.17.50.0/23 dst-address=10.37.175.200 log=no log-prefix="" 

 4    chain=input action=accept protocol=tcp dst-port=23 log=no log-prefix="" 

 5    chain=forward action=accept src-address=172.17.50.0/23 out-interface=inet log=no log-prefix="" 

 6    chain=input action=reject reject-with=icmp-network-unreachable protocol=tcp src-address=192.168.88.0/24 dst-port=23 log=no 
      log-prefix="" 

 7  D ;;; special dummy rule to show fasttrack counters
      chain=forward 

 8    ;;; defconf: accept ICMP
      chain=input action=accept protocol=icmp log=no log-prefix="" 

 9    ;;; defconf: accept establieshed,related
      chain=input action=accept connection-state=established,related log=no log-prefix="" 

10    ;;; defconf: drop all from WAN
      chain=input action=drop in-interface=eth1 log=no log-prefix="" 

11    ;;; defconf: fasttrack
      chain=forward action=fasttrack-connection connection-state=established,related log=no log-prefix="" 

12    ;;; defconf: accept established,related
      chain=forward action=accept connection-state=established,related log=no log-prefix="" 

13    ;;; defconf: drop invalid
      chain=forward action=drop connection-state=invalid log=no log-prefix="" 

14    ;;; defconf:  drop all from WAN not DSTNATed
      chain=forward action=drop connection-state=new connection-nat-state=!dstnat in-interface=eth1 log=no log-prefix="" 

15    chain=forward action=reject reject-with=icmp-network-unreachable src-address=172.17.50.0/23 log=no log-prefix="" 

# PRUEBA #

[root@j17 isx48102233]# ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: enp3s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 74:d0:2b:c9:f7:ac brd ff:ff:ff:ff:ff:ff
    inet 10.34.175.100/24 brd 10.34.175.255 scope global dynamic enp3s0
       valid_lft 657sec preferred_lft 657sec
    inet6 fe80::76d0:2bff:fec9:f7ac/64 scope link 
       valid_lft forever preferred_lft forever


[root@j17 isx48102233]# ping 10.33.175.100
PING 10.33.175.100 (10.33.175.100) 56(84) bytes of data.
From 10.34.175.1 icmp_seq=1 Destination Net Unreachable
From 10.34.175.1 icmp_seq=2 Destination Net Unreachable
From 10.34.175.1 icmp_seq=3 Destination Net Unreachable


# 8. Limitar ancho de banda de bajada en wireless a 100Kbps #

/queue tree
add limit-at=100k max-limit=100k name=cola_wifi packet-mark=no-mark parent=wlan1

# 9. Repartir por igual el tráfico entre todos los usuarios de deptA #
/queue tree
# Se puede poner packet-mark=no-mark si no hemos marcado conexiones y packetes
add name=deptA parent=deptA queue=pcq-download-default packet-mark=no-mark

# 10. Redirección de puerto server web #
/ip firewall nat add chain=dstnat dst-port=11175 protocol=tcp \
in-interface=inet action=dst-nat to-address=10.37.175.200 to-ports=80

#####################################################################
#																	#
#						  EXPORT DE MICROTIK						#
#																	#
#####################################################################

[admin@infMKT17Adri] > export
# may/25/2017 11:23:53 by RouterOS 6.33.5
# software id = X6F0-06BA
#
/interface wireless
set [ find default-name=wlan1 ] band=2ghz-b/g/n channel-width=20/40mhz-Ce disabled=no distance=indoors frequency=auto mode=ap-bridge \
    ssid=mkt17 wireless-protocol=802.11
/interface ethernet
set [ find default-name=ether1 ] name=eth1
set [ find default-name=ether2 ] name=eth2
set [ find default-name=ether3 ] name=eth3
set [ find default-name=ether4 ] name=eth4
/ip neighbor discovery
set eth1 discover=no
/interface vlan
add interface=eth4 name=deptA vlan-id=375
add interface=eth4 name=deptB vlan-id=475
add interface=eth3 name=inet vlan-id=80
add interface=eth4 name=servers vlan-id=775
/interface wireless security-profiles
set [ find default=yes ] supplicant-identity=MikroTik
/ip pool
add name=rangodeptA ranges=10.33.175.10-10.33.175.100
add name=rangodeptB ranges=10.34.175.10-10.34.175.100
add name=rangowireless ranges=172.17.50.10-172.17.51.250
/ip dhcp-server
add address-pool=rangodeptA disabled=no interface=deptA name=dhcp1
add address-pool=rangodeptB disabled=no interface=deptB name=dhcp2
add address-pool=rangowireless disabled=no interface=wlan1 name=dhcp3
/queue tree
add limit-at=100k max-limit=100k name=cola_wifi packet-mark=no-mark parent=wlan1
add name=deptA packet-mark=no-mark parent=deptA queue=pcq-download-default
/ip address
add address=192.168.88.1/24 comment=defconf interface=eth2 network=192.168.88.0
add address=10.33.175.1/24 interface=deptA network=10.33.175.0
add address=10.34.175.1/24 interface=deptB network=10.34.175.0
add address=10.37.175.1/24 interface=servers network=10.37.175.0
add address=192.168.3.175/16 interface=inet network=192.168.0.0
add address=10.30.0.175/24 interface=inet network=10.30.0.0
add address=172.17.50.1/23 interface=wlan1 network=172.17.50.0
/ip dhcp-server network
add address=10.33.175.0/24 dns-server=8.8.8.8 domain=deptA.com gateway=10.33.175.1 netmask=24
add address=10.34.175.0/24 dns-server=8.8.8.8 domain=deptB.com gateway=10.34.175.1 netmask=24
add address=172.17.50.0/23 dns-server=8.8.8.8 domain=wifi.com gateway=172.17.50.1 netmask=23
/ip firewall filter
add chain=forward connection-state=established,related
add action=reject chain=forward dst-address=10.34.175.0/24 packet-mark=no-mark src-address=10.33.175.0/24
add action=reject chain=forward dst-address=10.33.175.0/24 packet-mark=no-mark src-address=10.34.175.0/24
add chain=forward dst-address=10.37.175.200 src-address=172.17.50.0/23
add chain=input dst-port=23 protocol=tcp
add chain=forward out-interface=inet src-address=172.17.50.0/23
add action=reject chain=input dst-port=23 protocol=tcp src-address=192.168.88.0/24
add chain=input comment="defconf: accept ICMP" protocol=icmp
add chain=input comment="defconf: accept establieshed,related" connection-state=established,related
add action=drop chain=input comment="defconf: drop all from WAN" in-interface=eth1
add action=fasttrack-connection chain=forward comment="defconf: fasttrack" connection-state=established,related
add chain=forward comment="defconf: accept established,related" connection-state=established,related
add action=drop chain=forward comment="defconf: drop invalid" connection-state=invalid
add action=drop chain=forward comment="defconf:  drop all from WAN not DSTNATed" connection-nat-state=!dstnat connection-state=new \
    in-interface=eth1
add action=reject chain=forward src-address=172.17.50.0/23
/ip firewall mangle
add action=mark-connection chain=prerouting connection-state=new new-connection-mark=fromservers src-address=10.37.175.0/24
add action=mark-routing chain=prerouting in-interface=servers new-routing-mark=serverstoinetB
/ip firewall nat
add action=masquerade chain=srcnat out-interface=inet
add action=dst-nat chain=dstnat dst-port=11175 protocol=tcp to-addresses=10.37.175.200 to-ports=80
/ip route
add distance=1 gateway=10.30.0.1 routing-mark=serverstoinetB
add distance=1 gateway=192.168.0.1
add distance=2 gateway=10.30.0.1
/ip service
set ssh port=5322
set winbox port=5323
/system clock
set time-zone-name=Europe/Madrid
/system identity
set name=infMKT17Adri
/system routerboard settings
set cpu-frequency=650MHz protected-routerboot=disabled
/tool mac-server
set [ find default=yes ] disabled=yes
add interface=deptA
/tool mac-server mac-winbox
set [ find default=yes ] disabled=yes
add interface=deptA

#####################################################################
#																	#
#						  EXPORT DE SWITCH							#
#																	#
#####################################################################

Switch#show running-config 
Building configuration...

Current configuration : 27626 bytes
!
version 12.1
no service pad
service timestamps debug uptime
service timestamps log uptime
no service password-encryption
!
hostname Switch
!
!
ip subnet-zero
!
!
spanning-tree mode pvst
no spanning-tree optimize bpdu transmission
spanning-tree extend system-id
no spanning-tree vlan 2-1000
!
!
interface FastEthernet0/1
 switchport access vlan 80
 switchport mode access
 no ip address
!
interface FastEthernet0/2
 switchport trunk allowed vlan 80
 switchport mode trunk
 no ip address
!
interface FastEthernet0/3
 switchport trunk allowed vlan 375,475,775
 switchport mode trunk
 no ip address
!
interface FastEthernet0/4
 switchport access vlan 375
 switchport mode access
 no ip address
!
interface FastEthernet0/5
 switchport access vlan 475
 switchport mode access
 no ip address
!
interface FastEthernet0/6
 switchport access vlan 775
 switchport mode access
 no ip address
!
interface FastEthernet0/7
 no ip address
!
interface FastEthernet0/8
 no ip address
!
interface FastEthernet0/9
 no ip address
!
interface FastEthernet0/10
 no ip address
!
interface FastEthernet0/11
 no ip address
!
interface FastEthernet0/12
 no ip address
!
interface FastEthernet0/13
 no ip address
!
interface FastEthernet0/14
 no ip address
!
interface FastEthernet0/15
 no ip address
!
interface FastEthernet0/16
 no ip address
!
interface FastEthernet0/17
 no ip address
!         
interface FastEthernet0/18
 no ip address
!
interface FastEthernet0/19
 no ip address
!
interface FastEthernet0/20
 no ip address
!
interface FastEthernet0/21
 no ip address
!
interface FastEthernet0/22
 no ip address
!
interface FastEthernet0/23
 no ip address
!
interface FastEthernet0/24
 no ip address
!
interface Vlan1
 no ip address
 no ip route-cache
 shutdown
!
ip http server
!
!
line con 0
line vty 5 15
!
end
