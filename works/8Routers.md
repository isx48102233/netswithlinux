######### ROUTER 1 #############

hostname R1

interface Serial0/1/0
clock rate 128000
no shutdown
ip address 172.17.12.1 255.255.255.0

interface Serial0/1/1
clock rate 128000
no shutdown
ip address 172.17.15.1 255.255.255.0

######### ROUTER 2 #############

hostname R2

interface Serial0/1/0
clock rate 128000
no shutdown
ip address 172.17.23.1 255.255.255.0

interface Serial0/1/1
clock rate 128000
no shutdown
ip address 172.17.12.2 255.255.255.0

######### ROUTER 3 #############

hostname R3

interface Serial0/1/0
clock rate 128000
no shutdown
ip address 172.17.34.1 255.255.255.0

interface Serial0/1/1
clock rate 128000
no shutdown
ip address 172.17.23.2 255.255.255.0

######### ROUTER 4 #############

hostname R4

interface Serial0/1/0
clock rate 128000
no shutdown
ip address 172.17.48.1 255.255.255.0

interface Serial0/1/1
clock rate 128000
no shutdown
ip address 172.17.34.2 255.255.255.0

######### ROUTER 5 #############

hostname R5

interface Serial0/1/0
clock rate 128000
no shutdown
ip address 172.17.15.2 255.255.255.0

interface Serial0/1/1
clock rate 128000
no shutdown
ip address 172.17.56.1 255.255.255.0

######### ROUTER 6 #############

hostname R6

interface Serial0/1/0
clock rate 128000
no shutdown
ip address 172.17.56.2 255.255.255.0

interface Serial0/1/1
clock rate 128000
no shutdown
ip address 172.17.67.1 255.255.255.0

######### ROUTER 7 #############

hostname R7

interface Serial0/1/0
clock rate 128000
no shutdown
ip address 172.17.67.2 255.255.255.0

interface Serial0/1/1
clock rate 128000
no shutdown
ip address 172.17.78.1 255.255.255.0

######### ROUTER 8 #############

hostname R8

interface Serial0/1/0
clock rate 128000
no shutdown
ip address 172.17.78.2 255.255.255.0

interface Serial0/1/1
clock rate 128000
no shutdown
ip address 172.17.48.2 255.255.255.0
